<?php

//class
trait Hewan{

    //Properties:
    public $nama;
    public $darah = 50;
    public $kaki;
    public $keahlian;

    //Constructors:
    public function __construct($nama, $darah, $kaki, $keahlian){
        $this->nama = $nama;
        $this->darah = $darah;
        $this->kaki = $kaki;
        $this->keahlian = $keahlian;
    }

    //Methods:
    public function set_nama($nama){
        $this->nama = $nama;
    }

    public function get_nama(){
        return $this->nama;
    }

    public function set_darah($darah){
        $this->darah = $darah;
    }

    public function get_darah(){
        return $this->darah;
    }

    public function set_kaki($kaki){
        $this->kaki = $kaki;
    }

    public function get_kaki(){
        return $this->kaki;
    }

    public function set_keahlian($keahlian){
        $this->keahlian = $keahlian;
    }

    public function get_keahlian(){
        return $this->keahlian;
    }

    public function atraksi($keahlian){
        $this->atraksi = $keahlian;
    }
    
}

?>