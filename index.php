<?php

require_once 'Hewan.php';
require_once 'Fight.php';
require_once 'Elang.php';
require_once 'Harimau.php';

// $Elang = new Elang;
// $Harimau = new Harimau;

// $Elang->set_kaki(2);
// echo 'jumlahKaki: ' . $Elang->get_kaki;
// $Elang->set_keahlian('terbang tinggi');
// echo 'keahlian: ' . $Elang->get_keahlian;
// $Elang->set_attack(10);
// echo 'attackPower: ' . $Elang->get_attack;
// $Elang->set_deffence(5);
// echo 'deffencePower: ' . $Elang->get_deffence;

// $Harimau->set_kaki(4);
// echo 'jumlahKaki: ' . $Harimau->get_kaki;
// $Harimau->set_keahlian('lari cepat');
// echo 'keahlian: ' . $Harimau->get_keahlian;
// $Harimau->set_attack(7);
// echo 'attackPower: ' . $Harimau->get_attack;
// $Harimau->set_deffence(8);
// echo 'deffencePower: ' . $Harimau->get_deffence;


// $Hewan1 = new Hewan('elang', 50, 2, 'terbang');
// $Hewan2 = new Hewan('harimau', 50, 4, 'berlari');

$Elang = new Elang('Elang', 50, 2, 'terbang tinggi', 10, 5);
echo 'Nama Hewan: ' . $Elang->get_nama() . '<br>';
echo 'Jumlah Darah: ' . $Elang->get_darah() . '<br>';
echo 'Jumlah Kaki: ' . $Elang->get_kaki() . '<br>';
echo 'Keahlian: ' . $Elang->get_keahlian() . '<br>';
echo 'attackPower: ' . $Elang->get_attack() . '<br>';
echo 'deffencePower: ' . $Elang->get_deffence() . '<br><br>';

$Harimau = new Harimau('Harimau', 50, 4, 'lari cepat', 7, 8);
echo 'Nama Hewan: ' . $Harimau->get_nama() . '<br>';
echo 'Jumlah Darah: ' . $Harimau->get_darah() . '<br>';
echo 'Jumlah Kaki: ' . $Harimau->get_kaki() . '<br>';
echo 'Keahlian: ' . $Harimau->get_keahlian() . '<br>';
echo 'attackPower: ' . $Harimau->get_attack() . '<br>';
echo 'deffencePower: ' . $Harimau->get_deffence() . '<br><br>';

$Elang->set_keahlian('terbang tinggi');
$Harimau->set_keahlian('lari cepat');

echo $Elang->atraksi;
echo $Harimau->atraksi;

$Elang->serang('Elang_3 sedang menyerang Harimau_2');
$Harimau->serang('Harimau_1 sedang menyerang Elang_3');

echo $Elang->attackPower;
echo $Harimau->attackPower;

$Elang->diserang('Elang_3 sedang diserang');
$Harimau->diserang('Harimau_1 sedang diserang');

echo $Elang->deffencePower;
echo $Harimau->deffencePower;

//$Hewan1->set_nama();
// echo 'namanya..' .$Hewan1->get_nama() .'<br>';
// echo 'darahnya..' .$Hewan1->get_darah() .'<br>';
// echo 'kakinya..' .$Hewan1->get_kaki() .'<br>';
// echo 'keahliannya..' .$Hewan1->get_keahlian() .'<br>';

// //$Hewan2->set_nama();
// echo 'namanya..' .$Hewan2->get_nama() .'<br>';
// echo 'darahnya..' .$Hewan2->get_darah() .'<br>';
// echo 'kakinya..' .$Hewan2->get_kaki() .'<br>';
// echo 'keahliannya..' .$Hewan2->get_keahlian() .'<br>';

?>