<?php

//class
trait Fight{

    //Properties:
    public $attackPower;
    public $deffencePower;

    //Constructors:
    public function __construct($attackPower, $deffencePower){
        $this->attackPower = $attackPower;
        $this->deffencePower = $deffencePower;
    }
    
    //Methods:
    public function set_attack($attackPower){
        $this->attackPower = $attackPower;
    }

    public function get_attack(){
        return $this->attackPower;
    }

    public function set_deffence($deffencePower){
        $this->deffencePower = $deffencePower;
    }

    public function get_deffence(){
        return $this->deffencePower;
    }

    public function serang($attackPower){
        $this->attackPower = $attackPower;
    }

    public function diserang($deffencePower){
        $this->deffencePower = $deffencePower;        
    }
}

?>